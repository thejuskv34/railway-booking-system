/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.railway.booking.system.ui.model.data;

import java.sql.Date;

/**
 *
 * @author student
 */
public class Traveller {
    private String Name;
    private Integer Age;
    private String Gender;
    private Integer Phone;

    public Traveller(String Name, Integer Age, String Gender, Integer Phone, Integer TrainId, Integer TicketNumber, Double TotalPrice, Date Dob) {
        this.Name = Name;
        this.Age = Age;
        this.Gender = Gender;
        this.Phone = Phone;
        this.TrainId = TrainId;
        this.TicketNumber = TicketNumber;
        this.TotalPrice = TotalPrice;
        this.Dob = Dob;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer Age) {
        this.Age = Age;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public Integer getPhone() {
        return Phone;
    }

    public void setPhone(Integer Phone) {
        this.Phone = Phone;
    }

    public Integer getTrainId() {
        return TrainId;
    }

    public void setTrainId(Integer TrainId) {
        this.TrainId = TrainId;
    }

    public Integer getTicketNumber() {
        return TicketNumber;
    }

    public void setTicketNumber(Integer TicketNumber) {
        this.TicketNumber = TicketNumber;
    }

    public Double getTotalPrice() {
        return TotalPrice;
    }

    public void setTotalPrice(Double TotalPrice) {
        this.TotalPrice = TotalPrice;
    }

    public Date getDob() {
        return Dob;
    }

    public void setDob(Date Dob) {
        this.Dob = Dob;
    }
    private Integer TrainId;
    private Integer TicketNumber;
    private Double TotalPrice;
    private Date Dob;
}
