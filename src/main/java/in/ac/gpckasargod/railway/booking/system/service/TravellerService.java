/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasargod.railway.booking.system.service;

import in.ac.gpckasargod.railway.booking.system.ui.model.data.Traveller;
import java.awt.List;
import java.sql.Date;

/**
 *
 * @author student
 */
public interface TravellerService {
    public String saveTraveller(string name,Integer age,String gender,Integer phone,Integer trainid,Integer ticketnumber,Double totalprice,java.util.Date date);
    public Traveller readTraveller(Integer Id);
    public List<Traveller> getAllTraveller();
     
    public String updateTraveller(Integer selectedId,String txtname,int txtage,String txtgender,int txtphone,int txttrainid,int txtTicketNumber,int txttotalprice,Date travellingdate);
    public String deleteTraveller(Integer selectedId,String txtname,int txtage,String txtgender,int txtphone,int txttrainid,int txtTicketNumber,int txttotalprice,Date travellingdate);
    public String deleteTraveller(Integer id);

    public static class string {

        public string() {
        }
    }
}

    
