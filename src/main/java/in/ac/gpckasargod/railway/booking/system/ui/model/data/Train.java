/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.railway.booking.system.ui.model.data;

/**
 *
 * @author student
 */
public class Train {
    private String TrainName;
    private String Id;
    private String Startplace;
    private String Destination;
    private Double Price;
    private String Class;
    
    public Train(String TrainName,String Id,String Startplace,String Destination,Double Price,String Class){
        this.TrainName = TrainName;
        this.Id = Id;
        this.Startplace = Startplace;
        this.Destination = Destination;
        this.Price = Price;
        this.Class = Class;
    }

    public String getTrainName() {
        return TrainName;
    }

    public void setTrainName(String TrainName) {
        this.TrainName = TrainName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getStartplace() {
        return Startplace;
    }

    public void setStartplace(String Startplace) {
        this.Startplace = Startplace;
    }

    public String getDestination() {
        return Destination;
    }

    public void setDestination(String Destination) {
        this.Destination = Destination;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double Price) {
        this.Price = Price;
    }

    /**
     *
     * @return
     */
    public String getClass() {
        return Class;
    }

    public void setClass(String Class) {
        this.Class = Class;
    }
    
    
}
