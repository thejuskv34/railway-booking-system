/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasargod.railway.booking.system.service.impl;

import in.ac.gpckasargod.railway.booking.system.service.TravellerService;
import in.ac.gpckasargod.railway.booking.system.ui.model.data.Traveller;
import java.awt.List;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class TravellerServiceImpl extends ConnectionServiceImpl implements TravellerService {

    public String saveTraveller(String name, Integer age, String gender, Integer phone, Integer trainid, Integer ticketnumber, Double totalprice, java.util.Date travellingdate) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO TRAVELLER (NAME,AGE,GENDER,PHONE,TRAINID,TICKETNUMBER,TOTALPRICE,TRAVELLINGDATE) VALUES " + "('" +name+"','"+gender+"','"+age+"','"+phone+"','"+trainid+"','"+ticketnumber +"','"+totalprice+"','"+travellingdate+"')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "saved failed";
            } else {
                return "saved succesfuly";

            }

        } catch (SQLException ex) {
            Logger.getLogger(TravellerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "save failed";
    }

    ;
    

    public Traveller readTraveller(Integer Id) {
        Traveller traveller = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT *FROM TRAVELLER WHERE ID=+id";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                String travellerName = resultSet.getString("NAME");
                int travellerAge = resultSet.getInt("AGE");
                String travellerGender = resultSet.getString("GENDER");
                int travellerPhone = resultSet.getInt("PHONE");
                int travellerTrainId = resultSet.getInt("TRAINID");
                int travellerTicketNumber = resultSet.getInt("TICKETNUMBER");
                Double travellerTotalprice = resultSet.getDouble("TOTALPRICE");
                Date travellerTravellingDate = resultSet.getDate("TRAVELLINGDATE");
                traveller = new Traveller(travellerName, travellerAge, travellerGender, travellerPhone, travellerTrainId, travellerTicketNumber, travellerTotalprice, (java.sql.Date) travellerTravellingDate);

            }
        } catch (SQLException ex) {
            Logger.getLogger(TravellerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return traveller;
    }

    /**
     *
     * @return
     * @throws java.sql.SQLExceptioTravellerDetailsFormn
     */
    
    static {    

    /**
     *
     * @return
     * @throws SQLException
     * @throws SQLException
     */

    /**
     *
     * @return
     * @throws SQLException
     */
    @Override
    public List<Traveller> getAllTravellers() throws SQLException,SQLException {
        try {
            Connection connection = getConnection();
           
            Statement statement = connection.createStatement();
            String query = "SELECT *FROM TRAVELLER";
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
            String travellerName = resultSet.getString("NAME");
            int travellerAge = resultSet.getInt("AGE");
            String travellerGender = resultSet.getString("GENDER");
            int travellerPhone = resultSet.getInt("PHONE");
            int travellerTrainId = resultSet.getInt("TRAINID");
            int travellerTicketNumber = resultSet.getInt("TICKETNUMBER");
            Double travellerTotalPrice = resultSet.getDouble("TOTALPRICE");
            Date travellerTravellingDate = resultSet.getDate("TRAVELLINGDATE");
            Traveller traveller = new Traveller(travellerName,travellerAge,travellerGender,travellerPhone,travellerTrainId,travellerTicketNumber,travellerTotalPrice, (java.sql.Date) travellerTravellingDate);
            
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(TravellerServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }return null;
    }}


    
    
        
    
